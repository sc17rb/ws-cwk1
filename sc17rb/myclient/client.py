import requests
import json
import sys

URL = 'http://sc17rb.pythonanywhere.com/'
client = requests.session()
enum_rating = {0: "No Rating", 1:"*", 2:"**", 3:"***", 4:"****", 5:"*****"}


def register(user, passw, email):
    payload = {'user': user,
               'password': passw,
               'email': email}

    response = client.post(URL + 'register/', data=json.dumps(payload))
    decoded_response = response.content.decode('utf-8')

    if response.status_code == 201:
        print("Response code: {} - {}".format(
                response.status_code, response.reason))
    else:
        print("Response code: {} - {}.\nReason: {}".format(
                response.status_code, response.reason, decoded_response))


def login(user, passw, url):
    payload = {'user': user,
               'password': passw}

    # very basic url validation
    if url[-1] != '/':
        url = url + '/'

    response = client.post(url + 'login/', data=json.dumps(payload))
    decoded_response = response.content.decode('utf-8')

    if response.status_code == 200:
        print("Response code: {} - {}".format(
                response.status_code, response.reason))
    else:
        print("Response code: {} - {}.\nReason: {}".format(
                response.status_code, response.reason, decoded_response))


def logout():
    response = client.post(URL + 'logout/')
    decoded_response = response.content.decode('utf-8')

    if response.status_code == 200:
        print("Response code: {} - {}".format(
                response.status_code, response.reason))
    else:
        print("Response code: {} - {}.\nReason: {}".format(
                response.status_code, response.reason, decoded_response))


def list_modules():
    response = client.get(URL + 'list/')
    decoded_response = response.content.decode('utf-8')

    # Check if the response has a successful status code
    if response.status_code == 200:
        json_response = json.loads(decoded_response)

        # display response status
        print("Response: {} {}.\n".format(response.status_code, response.reason))
        # format and display the json response
        index = 1
        for module in json_response['module-instances']:
            print("{})".format(index))
            print("Code: {}".format(module['code']))
            print("Name: {}".format(module['name']))
            print("Year: {}".format(module['year']))
            print("Semester: {}".format(module['semester']))
            print("Taught by:")
            for professor in module['professors']:
                print("{}, Professor {}. {}".format(professor['id'],
                    professor['name'], professor['surname']))
            print("")
            index = index + 1
    else:
        # display response status with its content
        print("Response: {} {}.\nReason: {}".format(response.status_code,
            response.reason, decoded_response))


def view_avg_ratings():
    response = client.get(URL + 'view/')
    decoded_response = response.content.decode('utf-8')

    # Check if the response has a successful status code
    if response.status_code == 200:
        json_response = json.loads(decoded_response)

        # display response status
        print("Response: {} {}.\n".format(response.status_code, response.reason))

        # format and display the json response
        for professor in json_response['professor-ratings']:
            print("The rating of Professor {} ({}) is {}".format(professor['name'],
                professor['id'], enum_rating[professor['rating']]))
    else:
        # display response status with its content
        print("Response: {} {}.\nReason: {}".format(response.status_code,
            response.reason, decoded_response))


def average_rating_in_module(identifier, code):
    payload = {'professor_id': identifier,
               'module_code': code}

    response = client.get(URL + 'average/', data=json.dumps(payload))
    decoded_response = response.content.decode('utf-8')

    # Check if the response has a successful status code
    if response.status_code == 200:
        json_response = json.loads(decoded_response)

        # display response status
        print("Response: {} {}.\n".format(response.status_code, response.reason))

        # format and display the json response
        print("The rating of Professor {} ({}) in the module {} ({}) is {}".format(
            json_response['name'], identifier, json_response['module'], code,
            enum_rating[json_response['avg']]
        ))
    else:
        # display response status with its content
        print("Response: {} {}.\nReason: {}".format(response.status_code,
            response.reason, decoded_response))


def rate_professor(identifier, code, year, sem, rating):
    payload = {'professor_id': identifier,
               'module_code': code,
               'year': year,
               'semester': sem,
               'rating': rating}

    response = client.post(URL + 'rate/', data=json.dumps(payload))
    decoded_response = response.content.decode('utf-8')

    # Check if the response has a successful status code
    if response.status_code == 200:
        # display response status
        print("Response: {} {}.\n".format(response.status_code, response.reason))
    else:
        # display response status with its content
        print("Response: {} {}.\nReason: {}".format(response.status_code,
            response.reason, decoded_response))


if __name__ == "__main__":
    while True:
        print("Command: ", end="")
        command = input()
        if command == "register":
            print("Username:")
            user = input()
            print("Email:")
            email = input()
            print("Password:")
            passw = input()

            register(user, passw, email)
        elif "login" in command:
            # Validate the command and extract arguments
            if len(command.split()) == 2:
                url = command.split()[1]
            else:
                print("Incorrect format, use 'login url'!")
                continue

            print("Username:")
            user = input()
            print("Password:")
            passw = input()

            login(user, passw, url)
        elif command == "logout":
            logout()
        elif command == "list":
            list_modules()
        elif command == "view":
            view_avg_ratings()
        elif "average" in command:
            # Validate the command and extract arguments
            if len(command.split()) == 3:
                arg_list = command.split()
                professor_id = arg_list[1]
                module_code = arg_list[2]
            else:
                print("Incorrect format, use 'average professor_id module_code'!")
                continue

            average_rating_in_module(professor_id, module_code)
        elif "rate" in command:
            # Validate the command and extract arguments
            if len(command.split()) == 6:
                arg_list = command.split()
                professor_id = arg_list[1]
                module_code = arg_list[2]
                try:
                    year = int(arg_list[3])
                    sem = int(arg_list[4])
                    rating = int(arg_list[5])
                except ValueError:
                    print("Incorrect format, use 'rate professor_id module_code year semester rating'!")
                    continue
            else:
                print("Incorrect format, use 'rate professor_id module_code year semester rating'!")
                continue

            rate_professor(professor_id, module_code, year, sem, rating)
        elif command == "q":
            print("You have quit the client application.")
            break
        else:
            print("Incorrect command format! Use 'q' to quit the application.")
