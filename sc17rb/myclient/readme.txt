1. Commands:
1) Register a new user:
$ register

2) Login to the service:
$ login url
where url is: http://sc17rb.pythonanywhere.com/

3) Logout from the service:
$ logout

4) List all the module instances:
$ list

5) Display the avg rating of each professor:
$  view

6) Display the avg rating of a professor in a module:
$ average professor_id module_code
where professor_id is the unique identifier of a professor,
and the module_code is the unique identifier of a module

7) Rate the teaching of a professor in a specific module instance:
$ rate professor_id module_code academic_year semester rating
where academic_year, semester and rating are integer values and
rating is a value in range from 1 to 5
* Example:
$ rate TT1 PG1 2017 2 1
would give a rating of 1 star for professor with id TT1 in a module instance with
the module code PG1, running in academic year 2017, semester 2.

8) Quit the application:
$ q

2. Pythonanywhere domain: http://sc17rb.pythonanywhere.com/

3. Password for the admin account: Banana87

4. To use the client:
1) Set up and activate a virtual environment:
$ module add anaconda3
$ python3 -m venv env
$ source env/bin/activate
2) Install necessary modules:
$ pip install requests
3) Run the client:
$ python client.py

5. Versions used when testing at DEC-10:
1) Python version = 3.6.5
2) Django version = 3.0.4
