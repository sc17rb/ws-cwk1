from django.apps import AppConfig


class RateProfessorsConfig(AppConfig):
    name = 'rate_professors'
