from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError


# Create your models here.
class Professor(models.Model):
    identifier = models.CharField(primary_key=True, max_length=3)
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)

    def __str__(self):
        return "{}: {}, {}".format(self.identifier, 
            self.last_name, self.first_name)


class Module(models.Model):
    module_code = models.CharField(primary_key=True, max_length=10)
    name = models.CharField(max_length=60)

    def __str__(self):
        return "{}: {}".format(self.module_code, self.name)


class ModuleInstance(models.Model):
    semesters = [(1, "Semester 1"), (2, "Semester 2")]
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    academic_year = models.PositiveIntegerField()
    semester = models.PositiveIntegerField(choices=semesters)
    professors = models.ManyToManyField(Professor)

    def __str__(self):
        return "{} [{}|Sem {}]".format(self.module,
            self.academic_year, self.semester)
    

class Rating(models.Model):
    stars = [(1, "*"), (2, "**"), (3, "***"), (4, "****"), (5, "*****")]

    module_inst = models.ForeignKey(ModuleInstance, on_delete=models.CASCADE)
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(choices=stars)

    def save(self, *args, **kwargs):
        # Make sure that the selected professor actually teaches the module inst
        if self.professor not in self.module_inst.professors.all():
            raise ValidationError("The selected professor does not teach this module instance!")
        super(Rating, self).save(*args, **kwargs)

    def __str__(self):
        return "{} rated professor {} in module {} [{}|Sem {}] a {}".format(
            self.user.username, self.professor.identifier, 
            self.module_inst.module.module_code, self.module_inst.academic_year, 
            self.module_inst.semester, self.rating)
        


