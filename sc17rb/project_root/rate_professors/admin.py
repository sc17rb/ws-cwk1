from django.contrib import admin
from rate_professors.models import Professor, Module, Rating, ModuleInstance

# Register your models here.
admin.site.register(Professor)
admin.site.register(Module)
admin.site.register(ModuleInstance)
admin.site.register(Rating)