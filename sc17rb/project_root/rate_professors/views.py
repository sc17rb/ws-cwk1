from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
import json
from decimal import *
from django.contrib.auth.decorators import login_required
from rate_professors.models import ModuleInstance, Professor, Module, Rating


@csrf_exempt
def register(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a POST request
    if request.method != 'POST':
        return http_bad_response

    # decode and parse the data
    data = json.loads(request.body.decode('utf-8'))
    
    # check if the username and email are unique
    if User.objects.filter(username=data['user']) or User.objects.filter(email=data['email']):
        http_bad_response.content = "Username or Email already taken!"
        return http_bad_response

    # register the user with the received information
    User.objects.create_user(username=data['user'], email=data['email'],
            password=data['password'])

    # set up the successful http response
    http_response = HttpResponse()
    http_response.status_code = 201
    http_response.reason_phrase = 'Created'

    return http_response


@csrf_exempt
def login_user(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a POST request
    if request.method != 'POST':
        return http_bad_response
    
    # check if user is already logged in
    if request.user.is_authenticated:
        http_bad_response.content = "You're already logged in!"
        return http_bad_response
    
    # decode and parse the data
    data = json.loads(request.body.decode('utf-8'))

    # authenticate user with the obtained data
    user = authenticate(request, username=data['user'], password=data['password'])
    if user != None:
        login(request, user)
    else:
        http_bad_response.content = "Failed to authenticate!"
        return http_bad_response
    
    # set up the successful http response
    http_response = HttpResponse()
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'

    return http_response


@csrf_exempt
def logout_user(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a POST request
    if request.method != 'POST':
        return http_bad_response

    # Check if the user is logged in, log out if true
    if request.user.is_authenticated:
        logout(request)
    else:
        http_bad_response.content = "Not logged in!"
        return http_bad_response
    
    # set up the successful http response
    http_response = HttpResponse()
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'

    return http_response


def list_module_instances(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a GET request
    if request.method != 'GET':
        return http_bad_response

    # obtain all module instances
    mod_inst = ModuleInstance.objects.all()
    list_of_modules = []

    # iterate through module instances and add each instance to a list
    for item in mod_inst:
        # iterate through list of professors for each module instance
        # add them to a list of professors
        list_of_professors = []
        for professor in item.professors.all():
            list_of_professors.append({'id': professor.identifier,
                                       'name': professor.first_name,
                                       'surname': professor.last_name})
        # construct a dictionary record for each module instance
        record = {'code': item.module.module_code,
                  'name': item.module.name,
                  'year': item.academic_year,
                  'semester': item.semester,
                  'professors': list_of_professors}

        list_of_modules.append(record)
    
    # create a payload from the obtained data
    payload = {'module-instances': list_of_modules}
    
    # set up the successful http response
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'

    return http_response


def view_avg(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a GET request
    if request.method != 'GET':
        return http_bad_response

    # obtain all professors
    professors = Professor.objects.all()
    professor_ratings = []

    # iterate through professors and add each element to a list
    for professor in professors:
        ratings_sum = 0
        avg_rating = 0
        number_of_ratings = 0
        # obtain the total ratings for each professor
        for rating in professor.rating_set.all():
            ratings_sum += rating.rating
            number_of_ratings += 1
        if number_of_ratings > 0:
            avg_rating = ratings_sum / number_of_ratings
            # round the avg rating half up
            avg_rating = int(Decimal(avg_rating).quantize(Decimal('1.'), rounding=ROUND_HALF_UP))

        # construct a record for each professor
        record = {'name': professor.first_name + ". " + professor.last_name,
                  'id': professor.identifier,
                  'rating': avg_rating}

        # append the record to the payload
        professor_ratings.append(record)
        
    # create a payload from the obtained data
    payload = {'professor-ratings': professor_ratings}
    
    # set up the successful http response
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'

    return http_response


def average_rating_in_module(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a GET request
    if request.method != 'GET':
        return http_bad_response
    
    # decode and parse the request data
    data = json.loads(request.body.decode('utf-8'))

    # obtain the requested module
    module = Module.objects.filter(module_code=data['module_code'])
    # obtain the given professor
    professor = Professor.objects.filter(identifier=data['professor_id'])

    # check if both exist
    if module and professor:
        module = module[0]
        professor = professor[0]
        teaching_professors = set()
        module_inst = module.moduleinstance_set.all()

        # obtain the professors that teach this module
        for instance in module_inst:
            temp = list(instance.professors.all())
            for item in temp:
                teaching_professors.add(item)

        # check if the given professor actually teaches the given module
        if professor in teaching_professors:
            sum_of_ratings = 0
            num_of_ratings = 0
            average_rating = 0
            # iterate through the module instances and for each instance note
            # the rating of the given professor
            for instance in module_inst:
                if professor in instance.professors.all():
                    for rating in instance.rating_set.all():
                        if rating.professor == professor:
                            sum_of_ratings += rating.rating
                            num_of_ratings += 1

            # Deal with division by zero
            if num_of_ratings > 0:
                average_rating = sum_of_ratings / num_of_ratings
            
            # round the average rating half up
            average_rating = int(Decimal(average_rating).quantize(Decimal('1.'), rounding=ROUND_HALF_UP))
        else:
            http_bad_response.content = "The given professor does not teach this module!"
            return http_bad_response
    else:
        http_bad_response.content = "Module code or professor id not valid!"
        return http_bad_response

    # create a payload from the obtained data
    payload = {'name': professor.first_name + ". " + professor.last_name,
               'module': module.name,
               'avg': average_rating}
    
    # set up the successful http response
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'

    return http_response


@csrf_exempt
def rate_professor(request):
    # set up a bad response in case of an error
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_bad_response.status_code = 503
    http_bad_response.reason_phrase = 'Service Unavailable'

    # looking for a GET request
    if request.method != 'POST':
        return http_bad_response
    
    # only logged in users may rate professors
    if not request.user.is_authenticated:
        http_bad_response.content = "You must be logged in to rate professors!"
        return http_bad_response
    
    # decode and parse the request data
    data = json.loads(request.body.decode('utf-8'))

    # perform validation on the request arguments
    professor = Professor.objects.filter(identifier=data['professor_id'])
    if not professor:
        http_bad_response.content = "There is no professor with id {}".format(
            data['professor_id'])
        return http_bad_response

    module = Module.objects.filter(module_code=data['module_code'])
    if not module:
        http_bad_response.content = "There is no module with code {}".format(
            data['module_code'])
        return http_bad_response
    
    module_inst = module[0].moduleinstance_set.filter(academic_year=data['year'], 
                                                      semester=data['semester'])
    
    if not module_inst:
        http_bad_response.content = "Module {} did not run in sem {}, year {}".format(
            module[0].name, data['semester'], data['year']
        )
        return http_bad_response
    
    teaching_professors = module_inst[0].professors.all()
    if professor[0] not in teaching_professors:
        http_bad_response.content = "Given module instance was not taught by the given professor"
        return http_bad_response

    if data['rating'] not in range(1,6):
        http_bad_response.content = "Rating must be an integer in range 1-5!"
        return http_bad_response

    already_rated_by_user = Rating.objects.filter(user=request.user,
                                                  module_inst=module_inst[0],
                                                  professor=professor[0])

    if already_rated_by_user:
        http_bad_response.content = "You have already rated this professor on this module instance"
        return http_bad_response
    
    # now that all is validated create a rating from the arguments in the request
    Rating.objects.create(module_inst=module_inst[0],
                          professor=professor[0],
                          user=request.user,
                          rating=data['rating'])

    # set up the successful http response
    http_response = HttpResponse()
    http_response.status_code = 200
    http_response.reason_phrase = 'OK'

    return http_response
